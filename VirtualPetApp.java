import java.util.Scanner;
public class VirtualPetApp {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Bear[] sloth = new Bear[4];
		for(int i = 0; i < sloth.length;i++){
			sloth[i] = new Bear();
			System.out.println("Enter the food for the bear ");
			sloth[i].food = reader.nextLine();
			System.out.println("Enter the height of the bear ");
			sloth[i].height = Integer.parseInt(reader.nextLine());
			System.out.println("Enter the colour of the bear ");
			sloth[i].colour = reader.nextLine();
		}
		System.out.println(sloth[sloth.length -1].food);
		System.out.println(sloth[sloth.length -1].height);
		System.out.println(sloth[sloth.length -1].colour);
		System.out.println(sloth[0].huntFood());
		System.out.println(sloth[0].dancingBear());
	}
}
	